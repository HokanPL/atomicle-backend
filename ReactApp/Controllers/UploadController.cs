﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ReactApp.Data;
using ReactApp.Dtos;
using ReactApp.Models;
using ReactApp.Tokens;
using System;
using System.IO;

namespace ReactApp.Controllers
{
    [Route("api")]
    [ApiController]
    public class UploadController : Controller
    {
        private readonly IWebHostEnvironment env;

        public UploadController(IWebHostEnvironment env)
        {
            this.env = env;
        }


        [HttpPost("uploadavatar")]
        public IActionResult SaveAvatar()
        {
            try
            {
                var httpRequest = Request.Form;
                var postedFile = httpRequest.Files[0];
                string filename = postedFile.FileName;
                var physicalPath = env.ContentRootPath + "/UploadedFiles/Images/ProfileImages/" + filename;

                using (var stream = new FileStream(physicalPath, FileMode.Create))
                {
                    postedFile.CopyTo(stream);
                }

                return Ok("Avatar updated successfully.");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
