﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ReactApp.Data;
using ReactApp.Dtos;
using ReactApp.Models;
using ReactApp.Tokens;
using System;
using B = BCrypt.Net.BCrypt; 

namespace ReactApp.Controllers
{
    public class UserData
    {
        public int ID { get; set; }
        public int userID { get; set; }
        public string name { get; set; }
        public string firstName { get; set; }
        public string secondName { get; set; }
        public string thirdName { get; set; }
        public string birthDate { get; set; }
        public string country { get; set; }

        public string avatarName { get; set; }

        public UserData(UserDetails details, string avatarName)
        {
            ID = details.ID;
            userID = details.userID;
            name = details.name;
            firstName = details.firstName;
            secondName = details.secondName;
            thirdName = details.thirdName;
            birthDate = details.birthDate;
            country = details.country;
            this.avatarName = avatarName;
        }
    }


    [Route("api")]
    [ApiController]
    public class AuthController : Controller
    {
        private readonly IUserRepository repository;
        private readonly JwtService jwtService;

        public AuthController(IUserRepository repository, JwtService jwtService)
        {
            this.repository = repository;
            this.jwtService = jwtService;
        }

        [HttpPost("register")]
        public IActionResult Register(RegisterDto dto)
        {
            //initialize necessary objects

            User user = new User();
            UserDetails userDetails = new UserDetails();
            UserCredentials userCredentials = new UserCredentials();
            UserPermissions userPermissions = new UserPermissions();

            //assign proper values
            user.isLoggedIn = false;
            user.profileImage = "default.jpg";
            user.dateCreated = dto.DateCreated;
            user.rankPoints = 0;

            userDetails.birthDate = dto.BirthDate;
            userDetails.country = dto.Country;
            userDetails.firstName = dto.FirstName;
            userDetails.name = dto.Name;
            userDetails.secondName = dto.SecondName;
            userDetails.thirdName = dto.ThirdName;

            userCredentials.email = dto.Email;
            userCredentials.password = B.HashPassword(dto.Password);

            userPermissions.CanComment = false;
            userPermissions.CanPublish = false;
            userPermissions.CanRead = true;
            userPermissions.CanReport = false;
            userPermissions.isAccountConfirmed = true;

            try
            {

                User createdUser = repository.Create(user);
                userCredentials.user = createdUser;
                userDetails.user = createdUser;
                userPermissions.user = createdUser;

                repository.Create(userCredentials);
                repository.Create(userDetails);
                repository.Create(userPermissions);

                repository.SaveChanges();

                return Created("success", createdUser);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("login")]
        public IActionResult Login(LoginDto dto)
        {
            var userCreds = repository.GetUserCredentialsByEmail(dto.Email);
            if (userCreds == null || !B.Verify(dto.Password, userCreds.password))
                return BadRequest("Invalid login or password.");

            var jwt = jwtService.Generate(userCreds.userID);

            Response.Cookies.Append("jwt", jwt, new CookieOptions { HttpOnly = true });

            return Ok(userCreds);
        }

        [HttpGet("user")]
        public IActionResult GetUser()
        {
            try
            {
                var jwt = Request.Cookies["jwt"];

                var token = jwtService.Verify(jwt);

                int userId = int.Parse(token.Issuer);

                var userDetails = repository.GetUserDetailsById(userId);
                var userAvatar = repository.GetUserById(userId).profileImage;

                return Ok(new UserData(userDetails, userAvatar));
            }
            catch(Exception)
            {
                return Unauthorized();
            }
        }

        [HttpPost("logout")]
        public IActionResult Logout()
        {
            Response.Cookies.Delete("jwt");
            return Ok(new { message = "success" });
        }
        
        [HttpPut("updateuseravatar")]
        public IActionResult UpdateAvatarPath(AvatarDto dto)
        {
            try
            {
                repository.UpdateUserAvatar(dto.Id, dto.Filename);
                repository.SaveChanges();

                return Ok("Avatar updated successfully.");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
