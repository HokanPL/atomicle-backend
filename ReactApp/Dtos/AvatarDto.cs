﻿namespace ReactApp.Dtos
{
    public class AvatarDto
    {
        public int Id { get; set; }
        public string Filename { get; set; }

    }
}
