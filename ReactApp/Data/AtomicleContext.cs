﻿using Microsoft.EntityFrameworkCore;
using ReactApp.Models;

namespace ReactApp.Data
{
    public class AtomicleContext : DbContext
    {
        public AtomicleContext(DbContextOptions<AtomicleContext> options) : base(options)
        {

        }

        public DbSet<User> Users { get; set; }
        public DbSet<UserCredentials> UserCredentials { get; set; }
        public DbSet<UserDetails> UserDetails { get; set; }
        public DbSet<UserPermissions> UserPermissions { get; set; }
    }
}
