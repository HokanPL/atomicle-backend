﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ReactApp.Models
{
    public class User
    {
        public int userID { get; set; }
        public bool isLoggedIn { get; set; }
        public string dateCreated { get; set; }
        public int rankPoints { get; set; }
        public string profileImage { get; set; }
    }
}
