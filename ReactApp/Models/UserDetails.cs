﻿namespace ReactApp.Models
{
    public class UserDetails
    {
        public int ID { get; set; }
        public int userID { get; set; }
        public virtual User user { get; set; }
        public string name { get; set; }
        public string firstName { get; set; }
        public string secondName { get; set; }
        public string thirdName { get; set; }
        public string birthDate { get; set; }
        public string country { get; set; }
    }
}
