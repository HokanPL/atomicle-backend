﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ReactApp.Models
{
    public class UserPermissions
    {
        public int ID { get; set; }
        public int userID { get; set; }
        public virtual User user { get; set; }
        public bool isAccountConfirmed { get; set; }
        public bool CanRead { get; set; }
        public bool CanComment { get; set; }
        public bool CanPublish { get; set; }
        public bool CanReport { get; set; }
    }
}
