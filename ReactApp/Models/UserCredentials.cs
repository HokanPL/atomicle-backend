﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace ReactApp.Models
{
    public class UserCredentials
    {
        public int ID { get; set; }

        public int userID { get; set; }
        public virtual User user { get; set; }
        public string email { get; set; }
        [JsonIgnore] public string password { get; set; }
    }
}
